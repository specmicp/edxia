edxia/Glue
==========


Glue
----

`Glue`_ is a "Python library to explore relationships within and between related datasets". The `Glue documentation`_ is a good place to start to learn about this software.

.. _Glue: https://glueviz.org/
.. _Glue documentation: http://docs.glueviz.org/en/stable/

edxia plugin
------------

In the Glue interface, ``edxia`` is available as a plugin. 
The goal of the plugin is to load the dataset and provide some tools to analyse the composition and the distributions of the phases.
The separation of the phases, and the clustering of points is done by the user with the selection tools offered by Glue.

This process is the recommended way for new user to use ``edxia``.
A step-by-step process is provided in the corresponding `documentation`_ ..

.. _documentation: https://doi.org/10.5281/zenodo.3686067
