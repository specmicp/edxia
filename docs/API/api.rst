API
===

This sections contains the documentation of the API

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   core
   composite
   io


.. automodule:: edxia



