Composite module
----------------

.. automodule:: edxia.composite

Channels
^^^^^^^^

.. automodule:: edxia.composite.channels
    :members:

Composite
^^^^^^^^^

.. automodule:: edxia.composite.composite
    :members:

Segmentation
^^^^^^^^^^^^

.. automodule:: edxia.composite.segmentation
    :members:

