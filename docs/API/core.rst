Core module
-----------

.. automodule:: edxia.core

Experiment
^^^^^^^^^^

.. automodule:: edxia.core.experiment
    :members:

Map
^^^

.. automodule:: edxia.core.map
    :members:

