IO module
---------

.. automodule:: edxia.io

Loaders
^^^^^^^

.. automodule:: edxia.io.loader
    :members:
    :show-inheritance:

.. automodule:: edxia.io.loader.base_loader
    :members:
    
Raw IO
^^^^^^

.. automodule:: edxia.io.raw_io
    :members:
    :show-inheritance:
    
HDF5
^^^^

.. automodule:: edxia.io.hdf5
    :members:
 
