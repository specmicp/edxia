.. edxia documentation master file, created by
   sphinx-quickstart on Fri Jul 19 17:26:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

edxia's documentation
=====================

.. figure:: _static/logo.png
    :align: center

.. warning::
    The documentation is under construction !

.. tip::
    A step-by-step tutorial is available in a separate documentation: `edxia through the Glueviz interface <https://doi.org/10.5281/zenodo.3686067>`_.
    This is the recommended document for non-technical users !
    
.. toctree::
   :maxdepth: 2
   :caption: Contents:

   about
   install
   input_files
   main_concepts

   glue
   API/api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
