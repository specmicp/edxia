About
=====

edxia
-----

edxia is an image analysis framework for EDS/BSE hyperspectral maps. It is available as an API to develop your own tool adapte to your problems or as a plugin to `Glueviz`_ to be available to any researcher.

The code is developed by `Fabien Georget`_ in the `Laboratory of Construction Materials`_, at `EPFL`_, Lausanne, Switzerland.
This work is a collaboration with William Wilson (EPFL, University of Sherbrooke) and Karen Scrivener (EPFL).


.. _Glueviz: https://glueviz.org/
.. _Fabien Georget: mailto:fabien.georget@epfl.ch
.. _Laboratory of Construction Materials: https://lmc.epfl.ch/
.. _EPFL: https://www.epfl.ch/en/

Quickstart
----------

A step-by-step tutorial is available in a separate documentation: `edxia through the Glueviz interface <https://doi.org/10.5281/zenodo.3686067>`_.


Citing
------

If you use this work, please cite the following:

    - Fabien Georget, William Wilson and Karen Scrivener,  edxia. Zenodo. http://doi.org/10.5281/zenodo.3246902
    - Fabien Georget, William Wilson and Karen Scrivener, Comprehensive microstructure characterization from quantified SEM-EDS maps in cementitious materials (in preparation)







